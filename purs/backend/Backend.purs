module Backend where

import Hello

sayHello = hello

import Data.JSON
import Data.Tuple
import Data.Map

list = encode $ [fromList [Tuple "to" "a", Tuple "name" "A"],
                 fromList [Tuple "to" "b", Tuple "name" "B"],
                 fromList [Tuple "to" "c", Tuple "name" "C"]]
