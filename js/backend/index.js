var express = require("express");
process.chdir(__dirname + '/PS');
var Backend = require("Backend");
process.chdir(__dirname);

var app = express();

app.use(express.static(__dirname + '/../../res'));
app.use(express.static(__dirname + '/../../js/frontend'));
app.use(express.static(__dirname + '/../../bower_components'));

app.get('/api/hello', function(req, res) { res.json(Backend.sayHello) });
app.get('/api/list', function(req, res) { res.send(Backend.list) });

app.listen(8000);
