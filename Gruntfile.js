module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        commonSrc: [
            "purs/common/**/*.purs",
            "bower_components/purescript-*/src/**/*.purs",
            "bower_components/purescript-*/src/**/*.purs.hs"
        ],

        backendSrc: [
            "purs/backend/**/*.purs",
            "<%=commonSrc%>"
        ],

        backendDest: "js/backend/PS",

        frontendOnlySrc: [
            "purs/frontend/**/*.purs"
        ],

        frontendSrc: [
            "<%=frontendOnlySrc%>",
            "<%=commonSrc%>"
        ],

        frontendDest: "js/frontend/purescript.js",

        pscMake: {
            backend: {
                src: ["<%=backendSrc%>"],
                dest: "<%=backendDest%>"
            }
        },

        psc: {
            frontend: {
                options: {
                    module: ["<%=frontendOnlySrc.map(function(s){return /\\/([^/])\\.purs/.exec(s)[1]})%>"]
                },
                src: ["<%=frontendSrc%>"],
                dest: "<%=frontendDest%>"
            }
        },

        clean: {
            backend: ["<%=backendDest%>"],
            frontend: ["<%=frontendDest%>"]
        },

        express: {
            options: {
                script: "js/backend/index.js",
                background: false
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-purescript");
    grunt.loadNpmTasks("grunt-express-server");

    grunt.registerTask("make", ["pscMake", "psc"]);
    grunt.registerTask("run", ["express"]);
    grunt.registerTask("default", ["make"]);
}
